package com.ve.sigis.test;


import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.*;
import java.util.concurrent.TimeUnit;


public class Test4_contratos {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;

  @Before
  public void setUp() {
    driver = new ChromeDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  
  @After
  public void tearDown() {
    driver.quit();
  }
  
  @Test
  public void contratos() throws InterruptedException {
  
	  
	  
		driver.get("https://fospuca.sigisqa.com/login");
	    //driver.manage().window().setSize(new Dimension(1853, 1053));
	    driver.findElement(By.id("mat-input-0")).click();
	    driver.findElement(By.id("mat-input-0")).sendKeys("leonardo.libre@sigis.com.ve");
	    driver.findElement(By.id("mat-input-1")).sendKeys("1234");
	    driver.findElement(By.id("btnLogin")).click();
	    Thread.sleep(3000);    
	    driver.findElement(By.xpath("//mat-card-header/div[2]")).click();
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    driver.findElement(By.cssSelector(".btn-block")).click();
	    driver.findElement(By.cssSelector(".form-control")).click();
	    driver.findElement(By.cssSelector(".form-control")).click();
	    driver.findElement(By.cssSelector(".form-control")).sendKeys("100000889227");
	    driver.findElement(By.xpath("//button[contains(.,\'Aceptar\')]")).click();
	    Thread.sleep(5000);
	    //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    
	    /*{
		      WebElement element = driver.findElement(By.xpath("//button[contains(.,\'Aceptar\')]"));
		      	Actions builder = new Actions(driver);
		      	builder.moveToElement(element).perform();
		 }*/
	    
	    driver.findElement(By.cssSelector(".btn-block")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.xpath("//div/button")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.xpath("//li")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.cssSelector(".btn-success")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.xpath("//span[contains(.,\'Aceptar\')]")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.xpath( "//a[8]/div")).click();
  }
}