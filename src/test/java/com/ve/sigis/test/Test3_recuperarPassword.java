package com.ve.sigis.test;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class  Test3_recuperarPassword {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  
  @Before
  public void setUp() {
    driver = new ChromeDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  
  @After
  public void tearDown() {
    driver.quit();
  }
  
  @Test
  public void recuperarPassword() throws InterruptedException {
	  
	    driver.get("https://fospuca.sigisqa.com/login");
	    driver.manage().window().setSize(new Dimension(1853, 1053));
	    driver.findElement(By.cssSelector(".mat-button-wrapper")).click();
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    driver.get("https://fospuca.sigisqa.com/recovery-password");
	    driver.findElement(By.xpath ("// input")).click();
	    driver.findElement(By.xpath("// input")).sendKeys("leonardo.libre@sigis.com.ve");
	    Thread.sleep(3000);
	    {
	      WebElement element = driver.findElement(By.cssSelector(".mat-primary:nth-child(1)"));
	      	Actions builder = new Actions(driver);
	      	builder.moveToElement(element).perform();
	    }
	    driver.findElement(By.xpath("//button")).click();
	    Thread.sleep(3000);
	  }
	}
