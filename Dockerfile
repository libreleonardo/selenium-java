	FROM openjdk:8-jre-slim
	
	ARG PRODUCT_NAME=fospuca
	ARG SERVICE_NAME=fospuca-test 
	ARG WORKDIR_NAME=/SIGIS/$PRODUCT_NAME/$SERVICE_NAME/
	ARG VERSION=${VERSION:-1.0.0}
	ARG MAINTAINER=${MAINTAINER:-llibre}
	
	LABEL maintainer="$MAINTAINER" \
	        org.opencontainers.image.authors="$MAINTAINER" \
	        org.opencontainers.image.source="http://git.sigis.com.ve/$PRODUCT_NAME/$SERVICE_NAME" \
	        org.opencontainers.image.version="$VERSION" \
	        org.opencontainers.image.vendor="SIGIS Soluciones Integrales GIS, C.A." \
	        org.opencontainers.image.title="$PRODUCT_NAME-$SERVICE_NAME" \
	        org.opencontainers.image.description="Docker for $PRODUCT_NAME $SERVICE_NAME"
	
	WORKDIR $WORKDIR_NAME
	
	COPY dist.
	EXPOSE 4000
	
	CMD ["java", "-jar", "fospuca.jar"]
