node {
    def image
    def DOCKER_HOST="unix:///var/run/docker.sock"

    try{

        stage ('Checkout') {
            updateGitlabCommitStatus name: 'build', state: 'pending'
            checkout scm
        }

        stage ('Maven') {                      
            updateGitlabCommitStatus name: 'build', state: 'running'
            docker.withServer('unix:///var/run/docker.sock') {              
                image= docker.image("maven:8-jdk-slim")
                image.inside(){ c ->
                    sh 'maven jar'                 
                }
            }
        }

        stage ('Test') {                        
            updateGitlabCommitStatus name: 'build', state: 'running'
            docker.withServer(DOCKER_HOST) {
                 image.inside(){ c ->
                    if(env.gitlabActionType == 'PUSH') {
                        sh 'maven build && ./test/java/JUnit --testsuite unit'
                    }else if(env.gitlabActionType == 'MERGE') {
                        sh 'maven build && ./test/java/JUnit --testsuite integration'
                        sh 'maven build && ./test/java/JUnit --testsuite funtional'
                    }
                       junit 'test-results.xml'
                }
            }
        }

        stage ('Build') {                       
            docker.withServer('unix:///var/run/docker.sock') {
                image = docker.build("${PRODUCT_NAME}/${SERVICE_NAME}-docker",
                        "--build-arg PRODUCT_NAME=${PRODUCT_NAME} "+
                        "--build-arg SERVICE_NAME=${SERVICE_NAME} "+
                        "--build-arg MAINTAINER=${gitlabUserEmail} "+
                        "--build-arg VERSION=${env.BUILD_NUMBER} .")
                
            }
        }

        stage ('Registry') {
            if(env.gitlabActionType == 'MERGE') {   
                docker.withServer('unix:///var/run/docker.sock') {                  
                    docker.withRegistry('https://registry.sigis.com.ve') {
                        image.push("${env.BUILD_NUMBER}")
                        image.push("latest")
                    }                   
                }
             
                acceptGitLabMR mergeCommitMessage: "Jenkins Accepted Merge of ${gitlabSourceBranch} Build#${env.BUILD_NUMBER}"
        }
      }

        currentBuild.result = 'SUCCESS'
        updateGitlabCommitStatus name: 'build', state: 'success'        
    } catch (err) {
        currentBuild.result = 'FAILURE'
        updateGitlabCommitStatus name: 'build', state: 'failed'
        	echo err.toString()
    } finally {
        step([$class: 'Mailer', 
            notifyEveryUnstableBuild: true, 
            recipients: "${gitlabUserEmail}, ${DEFAULT_MAIL}",
            sendToIndividuals: true]
        )
    }       
}